<?php

use yii\db\Migration;

class m160908_093815_create_junction_table_for_profile_and_resource_tables extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%profile_resource}}', [
            'user_id' => $this->integer(),
            'resource_id' => $this->integer(),
            'needs_time' => $this->integer()->defaultValue(0),
            'amount' => $this->integer(),
            'PRIMARY KEY(user_id, resource_id)',
        ], $tableOptions);

        $this->insert('{{%profile_resource}}', [
            'user_id' => '1',
            'resource_id' => '1',
            'needs_time' => 0,
            'amount' => 1000000,
        ]);
        $this->insert('{{%profile_resource}}', [
            'user_id' => '1',
            'resource_id' => '2',
            'needs_time' => 0,
            'amount' => 1000,
        ]);
        $this->insert('{{%profile_resource}}', [
            'user_id' => '1',
            'resource_id' => '3',
            'needs_time' => 0,
            'amount' => 1000,
        ]);
        $this->insert('{{%profile_resource}}', [
            'user_id' => '1',
            'resource_id' => '4',
            'needs_time' => 0,
            'amount' => 1000,
        ]);
        $this->insert('{{%profile_resource}}', [
            'user_id' => '1',
            'resource_id' => '5',
            'needs_time' => 0,
            'amount' => 1000,
        ]);

        $this->createIndex(
            'idx-profile_resource-user_id',
            '{{%profile_resource}}',
            'user_id'
        );

        $this->addForeignKey(
            'fk-profile_resource-user_id',
            '{{%profile_resource}}',
            'user_id',
            '{{%profile}}',
            'user_id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-profile_resource-resource_id',
            '{{%profile_resource}}',
            'resource_id'
        );

        $this->addForeignKey(
            'fk-profile_resource-resource_id',
            '{{%profile_resource}}',
            'resource_id',
            '{{%resource}}',
            'resource_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-profile_resource-user_id',
            '{{%profile_resource}}'
        );

        $this->dropIndex(
            'idx-profile_resource-user_id',
            '{{%profile_resource}}'
        );

        $this->dropForeignKey(
            'fk-profile_resource-resource_id',
            '{{%profile_resource}}'
        );

        $this->dropIndex(
            'idx-profile_resource-resource_id',
            '{{%profile_resource}}'
        );

        $this->dropTable('{{%profile_resource}}');
    }
}
