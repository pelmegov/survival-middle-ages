<?php

use yii\db\Migration;

class m160910_073014_create_chat_table extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%chat}}', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer()->notNull(),
            'message' => $this->text(),
            'updateDate' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], $tableOptions);

        $this->insert('{{%chat}}', [
            'id' => '1',
            'userId' => '1',
            'message' => 'Всем привет! Это первое сообщение! ',
            'updateDate' => '2016-09-05 20:14:13'
        ]);

        $this->insert('{{%chat}}', [
            'id' => '2',
            'userId' => '1',
            'message' => 'Здорова, как дела? =)))',
            'updateDate' => '2016-09-10 12:34:09'
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%chat}}');
    }
}
