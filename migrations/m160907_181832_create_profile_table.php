<?php

use yii\db\Migration;

class m160907_181832_create_profile_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%profile}}', [
            'user_id' => $this->primaryKey(),
            'avatar_id' => $this->integer() . '(11) NOT NULL DEFAULT 1',
            'nickname' => $this->string()->notNull(),
            'is_work' => $this->integer() . '(1) NOT NULL DEFAULT 0',
        ], $tableOptions);

        $this->insert('{{%profile}}', [
            'user_id' => '1',
            'avatar_id' => '2',
            'nickname' => 'ADMINISTRATOR',
            'is_work' => '0'
        ]);

        $this->addForeignKey('avatar_profile', '{{%profile}}', 'avatar_id', '{{%avatar}}', 'id');

        $this->createIndex('idx-profile-nickname', '{{%profile}}', 'nickname');

        $this->addForeignKey('profile_user', '{{%profile}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
    }

    public function down()
    {
        $this->dropForeignKey('avatar_profile', '{{%profile}}');
        $this->dropForeignKey('profile_user', '{{%profile}}');
        $this->dropIndex('idx-profile-nickname', '{{%profile}}');
        $this->dropTable('{{%profile}}');
    }
}
