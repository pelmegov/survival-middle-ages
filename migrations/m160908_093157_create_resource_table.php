<?php

use yii\db\Migration;

class m160908_093157_create_resource_table extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%resource}}', [
            'resource_id' => $this->primaryKey(),
            'resource_name' => $this->string()->notNull(),
            'needs_time' => $this->integer()->notNull(),
            'amount' => $this->integer()->notNull(),
            'gold_ratio' => $this->integer()->notNull(),
            'link_bg_image' => $this->string(),
            'link_image' => $this->string(),
        ], $tableOptions);

        $this->insert('{{%resource}}', [
            'resource_id' => '1',
            'resource_name' => 'Золото',
            'needs_time' => '0',
            'amount' => '0',
            'gold_ratio' => '0',
            'link_bg_image' => '/images/default_bg.jpg',
            'link_image' => '/images/res/gold.png',
        ]);
        $this->insert('{{%resource}}', [
            'resource_id' => '2',
            'resource_name' => 'Дерево',
            'needs_time' => '10',
            'amount' => '3',
            'gold_ratio' => '3',
            'link_bg_image' => '/images/wood.jpg',
            'link_image' => '/images/res/wood.png',
        ]);
        $this->insert('{{%resource}}', [
            'resource_id' => '3',
            'resource_name' => 'Камень',
            'needs_time' => '15',
            'amount' => '4',
            'gold_ratio' => '6',
            'link_bg_image' => '/images/stone.jpg',
            'link_image' => '/images/res/stone.png',
        ]);
        $this->insert('{{%resource}}', [
            'resource_id' => '4',
            'resource_name' => 'Рыба',
            'needs_time' => '20',
            'amount' => '2',
            'gold_ratio' => '16',
            'link_bg_image' => '/images/fishing.jpg',
            'link_image' => '/images/res/fish.png',
        ]);
        $this->insert('{{%resource}}', [
            'resource_id' => '5',
            'resource_name' => 'Шкуры животных',
            'needs_time' => '30',
            'amount' => '1',
            'gold_ratio' => '49',
            'link_bg_image' => '/images/hunting.jpg',
            'link_image' => '/images/res/lither.png',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%resource}}');
    }
}
