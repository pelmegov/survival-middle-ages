<?php

use yii\db\Migration;

class m160910_073016_create_task_tracker_table extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%task_tracker}}', [
            'id' => $this->primaryKey(),
            'profile_id' => $this->integer(),
            'title' => $this->string()->notNull(),
            'text' => $this->text(),
            'status' => $this->integer(),
            'responsible' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->insert('{{%task_tracker}}', [
            'id' => '1',
            'profile_id' => '1',
            'title' => "Таск #1",
            'text' => "Тест таска №1.",
            'status' => 1,
            'responsible' => 1,
            'created_at' => time(),
        ]);

        $this->addForeignKey('task_profile', '{{%task_tracker}}', 'profile_id', '{{%profile}}', 'user_id', 'cascade', 'cascade');
    }

    public function down()
    {
        $this->dropForeignKey('task_profile', '{{%task_tracker}}');
        $this->dropTable('{{%task_tracker}}');
    }
}
