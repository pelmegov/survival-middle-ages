<?php

use yii\db\Migration;

/**
 * Handles the creation for table `avatar`.
 */
class m160906_090005_create_avatar_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%avatar}}', [
            'id' => $this->primaryKey(),
            'link' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'cost' => $this->integer(),
        ], $tableOptions);

        $this->insert('{{%avatar}}', [
            'id' => '1',
            'link' => 'images/avatars/0.jpg',
            'name' => 'Человек без имени',
            'cost' => '0'
        ]);
        $this->insert('{{%avatar}}', [
            'id' => '2',
            'link' => 'images/avatars/1.png',
            'name' => 'Рыцарь',
            'cost' => '550'
        ]);
        $this->insert('{{%avatar}}', [
            'id' => '3',
            'link' => 'images/avatars/2.jpg',
            'name' => 'Чумной доктор',
            'cost' => '760'
        ]);
        $this->insert('{{%avatar}}', [
            'id' => '4',
            'link' => 'images/avatars/3.jpg',
            'name' => 'Вор',
            'cost' => '700'
        ]);
        $this->insert('{{%avatar}}', [
            'id' => '5',
            'link' => 'images/avatars/4.jpg',
            'name' => 'Миледи',
            'cost' => '650'
        ]);
        $this->insert('{{%avatar}}', [
            'id' => '6',
            'link' => 'images/avatars/5.png',
            'name' => 'Лекарь',
            'cost' => '350'
        ]);
        $this->insert('{{%avatar}}', [
            'id' => '7',
            'link' => 'images/avatars/6.jpg',
            'name' => 'Кузнец',
            'cost' => '400'
        ]);
        $this->insert('{{%avatar}}', [
            'id' => '8',
            'link' => 'images/avatars/7.jpg',
            'name' => 'Торговец',
            'cost' => '500'
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%avatar}}');
    }
}
