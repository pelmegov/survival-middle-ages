<?php

use yii\db\Migration;

class m160907_105831_create_user_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'username' => $this->string()->notNull(),
            'auth_key' => $this->string(32),
            'email_confirm_token' => $this->string(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string(),
            'email' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'role' => $this->integer()->notNull()->defaultValue(10),
        ], $tableOptions);

        $this->insert('{{%user}}', [
            'id' => '1',
            'created_at' => time(),
            'updated_at' => time(),
            'username' => 'admin',
            'auth_key' => '3nOgoJfv133Lr01zmgx_YzaeItmSylLx',
            'password_hash' => '$2y$13$lzqZ3tbLMemSDh1.AvezbOudvA/uhlVIOJRkYE080Akgryhz1NIBO',
            'email' => 'modkomi@mail.ru',
            'status' => '1',
            'role' => '30',
        ]);

        $this->createIndex('idx-user-username', '{{%user}}', 'username');
        $this->createIndex('idx-user-email', '{{%user}}', 'email');
        $this->createIndex('idx-user-status', '{{%user}}', 'status');
    }

    public function down()
    {
        $this->dropIndex('idx-user-username', '{{%user}}');
        $this->dropIndex('idx-user-email', '{{%user}}');
        $this->dropIndex('idx-user-status', '{{%user}}');
        $this->dropTable('{{%user}}');
    }
}
