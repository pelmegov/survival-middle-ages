<?php

namespace app\modules\task_tracker\models;

use app\modules\profile\models\Profile;
use Yii;

/**
 * This is the model class for table "game_task_tracker".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property string $title
 * @property string $text
 * @property integer $status
 * @property integer $responsible
 * @property integer $created_at
 *
 * @property Profile $profile
 */
class TaskTracker extends \yii\db\ActiveRecord
{

    const STATUS_OPENED = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_CLOSED = 3;
    const STATUS_IGNORED = 4;

    public static function tableName()
    {
        return '{{%task_tracker}}';
    }

    public function rules()
    {
        return [
            [['profile_id', 'status', 'responsible', 'created_at'], 'integer'],
            [['title', 'created_at'], 'required'],
            [['text'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['profile_id' => 'user_id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Ид пользователя',
            'responsible' => 'Ответственный',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'status' => 'Статус',
            'created_at' => 'Дата создания',
        ];
    }

    public static function getTaskStatus($status)
    {
        $statuses = self::getStatusesArray();
        return $statuses[$status] ? $statuses[$status] : "Что то не так со статусом.";
    }

    public static function getStatusesArray()
    {
        return [
            self::STATUS_OPENED => "Задача открыта",
            self::STATUS_ACTIVE => "Задача на выполнении",
            self::STATUS_CLOSED => "Задача закрыта",
            self::STATUS_IGNORED => "Задача отклонена",
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'profile_id']);
    }
}
