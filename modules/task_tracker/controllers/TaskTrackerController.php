<?php

namespace app\modules\task_tracker\controllers;

use app\modules\profile\models\Profile;
use app\modules\user\models\User;
use Yii;
use app\modules\task_tracker\models\TaskTracker;
use app\modules\task_tracker\models\TaskTrackerSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaskTrackerController implements the CRUD actions for TaskTracker model.
 */
class TaskTrackerController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [

                    /* Для администраторов */
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->username);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaskTracker models.
     * @return mixed
     */
    public function actionIndex()
    {

        $admins = Profile::getAdminProfiles();
        $searchModel = new TaskTrackerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'admins' => $admins,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    /**
     * Displays a single TaskTracker model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $admins = Profile::getAdminProfiles();
        return $this->render('view', [
            'admins' => $admins,
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TaskTracker model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaskTracker();

        $admins = Profile::getAdminProfiles();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'admins' => $admins,
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaskTracker model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $admins = Profile::getAdminProfiles();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'admins' => $admins,
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaskTracker model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TaskTracker model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaskTracker the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaskTracker::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
