<?php
/* @var $this yii\web\View */
use app\modules\task_tracker\models\TaskTracker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $model app\modules\task_tracker\models\TaskTracker */
/* @var $form yii\widgets\ActiveForm */
/* @var $admins */
?>

<div class="task-tracker-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'profile_id')->label(false)->hiddenInput(['value' => Yii::$app->user->id]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList(TaskTracker::getStatusesArray()); ?>

    <?
//    $admins = User::getAdmins();
//    foreach ($admins as $admin) {
//        $administrators[] = Profile::findOne(["user_id" => $admin->id]);
//    }
    // формируем массив, с ключем равным полю 'id' и значением равным полю 'name'
    $items = ArrayHelper::map($admins, 'user_id', 'nickname');
    $params = [
        'prompt' => 'Укажите ответственного'
    ];
    echo $form->field($model, 'responsible')->dropDownList($items, $params);
    ?>

    <?= $form->field($model, 'created_at')->label(false)->hiddenInput(['value' => time()]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>