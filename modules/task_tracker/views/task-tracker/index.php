<?php

use app\modules\task_tracker\models\TaskTracker;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\task_tracker\models\TaskTrackerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $admins */

$this->title = 'Task Trackers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-tracker-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создание задачи', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'profile.nickname',
            [
                'attribute' => 'responsible',
                'value' => function ($model) use ($admins) {
                    foreach ($admins as $admin) {
                        if ($model->responsible == $admin->user_id) {
                            return $admin->nickname;
                        }
                    }
                    return "Неизвестный юзер";
                },
            ],
            'title',
            [
                'attribute' => 'text',
                'value' => function ($model) {
                    return StringHelper::truncate($model->text, 100);
                }
            ],
            [
                'attribute' => 'status',
                'filter' => TaskTracker::getStatusesArray(),
                'value' => function ($model) {
                    return TaskTracker::getTaskStatus($model['status']);
                },
            ],
            'created_at:datetime',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
