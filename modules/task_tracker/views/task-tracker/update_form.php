<?php
use app\modules\task_tracker\models\TaskTracker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\modules\task_tracker\models\TaskTracker */
/* @var $form yii\widgets\ActiveForm */
/* @var $admins */
?>

<div class="task-tracker-task-tracker-update">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList(TaskTracker::getStatusesArray()); ?>

    <?
    // формируем массив, с ключем равным полю 'id' и значением равным полю 'name'
    $items = ArrayHelper::map($admins, 'user_id', 'nickname');
    $params = [
        'prompt' => 'Укажите ответственного'
    ];
    echo $form->field($model, 'responsible')->dropDownList($items, $params);
    ?>

    <?= $form->field($model, 'created_at')->label(false)->hiddenInput(['value' => time()]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>