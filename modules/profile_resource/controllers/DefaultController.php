<?php

namespace app\modules\profile_resource\controllers;

use app\modules\profile\models\Profile;
use app\modules\profile_resource\models\BuyForm;
use app\modules\profile_resource\models\SellForm;
use app\modules\profile_resource\models\ProfileResource;
use app\modules\user\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * Default controller for the `profile_resource` module
 */
class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    /* Для всех */
                    [
                        'allow' => true,
                        'controllers' => ['profile_resource/default'],
                        'actions' => ['test']
                    ],
                    /* Для гостей */
                    [
                        'allow' => true,
                        'controllers' => ['profile_resource/default'],
                        'actions' => ['test'],
                        'roles' => ['?'],
                    ],
                    /* Для зарегистрированных пользователей */
                    [
                        'allow' => true,
                        'controllers' => ['profile_resource/default'],
                        'actions' => ['market-resources', 'get-sum', 'get-col', 'get-cost'],
                        'roles' => ['@'],
                    ],
                    /* Для администраторов */
                    [
                        'allow' => true,
                        'controllers' => ['profile_resource/default'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->username);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetCol()
    {
        $prod_id = Yii::$app->request->post('id');
        $profile_resource = ProfileResource::findOne([
            "user_id" => Yii::$app->user->id,
            "resource_id" => $prod_id
        ]);
        $gold_ratio = $profile_resource->resource->gold_ratio;
        $gold = ProfileResource::findOne([
            "user_id" => Yii::$app->user->id,
            "resource_id" => 1
        ]);
        $isBuy = $gold->amount >= $gold_ratio;
        if ($isBuy) {
            return floor($gold->amount / $gold_ratio);
        }
        return 0;
    }

    public function actionGetCost()
    {
        $id = Yii::$app->request->post('id');
        $model = ProfileResource::findOne([
            "user_id" => Yii::$app->user->id,
            "resource_id" => $id
        ]);
        return $model->amount;
    }

    public function actionGetSum()
    {
        $prod_id = Yii::$app->request->post('prod_id');
        $col = Yii::$app->request->post('col');
        if ($col < 0) return 0;
        $profile_resource = ProfileResource::findOne([
            "user_id" => Yii::$app->user->id,
            "resource_id" => $prod_id
        ]);
        $gold_ratio = $profile_resource->resource->gold_ratio;
        $isBuy = $profile_resource->amount - $col >= 0 ? true : false;
        if ($isBuy) {
            $result = floor((($col * $gold_ratio) * 0.80));
        }
        return $result;
    }

    public function actionMarketResources()
    {
        $request = Yii::$app->request;
        $action = $request->get('action');
        $profile_resource = ProfileResource::findOne(Yii::$app->user->id);
        if ($action == "buy") {
            $model = new BuyForm();
        } else if ($action == "sell") {
            $model = new SellForm();
        } else {
            return $this->actionMarketResources();
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($action == "buy") {
                $profile_resource->buy($model->id, $model->amount);
            }
            if ($action == "sell") {
                $profile_resource->sell($model->id, $model->amount);
            }
            return $this->render('market-resources', [
                'model' => $model,
                'action' => $action
            ]);
        } else {
            return $this->render('market-resources', [
                'model' => $model,
                'action' => $action
            ]);
        }
    }

}
