<?php

namespace app\modules\profile_resource\models;

use app\modules\profile\models\Profile;
use app\modules\resource\models\Resource;
use Yii;

/**
 * This is the model class for table "{{%profile_resource}}".
 *
 * @property integer $user_id
 * @property integer $resource_id
 * @property integer $needs_time
 * @property integer $amount
 *
 * @property Resource $resource
 * @property Profile $user
 */
class ProfileResource extends \yii\db\ActiveRecord
{
    public $resources;

    public static function tableName()
    {
        return '{{%profile_resource}}';
    }

    /**
     * Покупка ресурсов
     * @param $id
     * @param $amount
     * @return array|bool
     */
    public function buy($id, $amount)
    {
        $profile = Profile::findOne(Yii::$app->user->id);
        $profile_resource = ProfileResource::find()
            ->where([
                "user_id" => $profile->user_id,
                "resource_id" => $id,
            ])
            ->andWhere("resource_id>1")
            ->one();
        $gold = ProfileResource::findOne([
            "user_id" => $profile->user_id,
            "resource_id" => 1
        ]);
        $isBuy = $gold->amount >= $amount * $profile_resource->resource->gold_ratio ? true : false;
        if ($isBuy) {
            $profile_resource->amount += $amount;
            $gold->amount -= $amount * $profile_resource->resource->gold_ratio;
            $gold->save();
            $profile_resource->save();
        }
        return $profile_resource ? [$profile_resource->amount, $gold->amount] : false;
    }

    /**
     * Продажа ресурсов
     * @param $id
     * @param $amount
     * @return array|bool
     */
    public function sell($id, $amount)
    {
        $profile = Profile::findOne(Yii::$app->user->id);
        $profile_resource = ProfileResource::find()
            ->where([
                "user_id" => $profile->user_id,
                "resource_id" => $id,
            ])
            ->andWhere("resource_id>1")
            ->one();
        $gold = ProfileResource::findOne([
            "user_id" => $profile->user_id,
            "resource_id" => 1
        ]);
        $isBuy = $profile_resource->amount - $amount >= 0 ? true : false;
        if ($isBuy) {
            $profile_resource->amount -= $amount;
            $gold->amount += floor((($amount * $profile_resource->resource->gold_ratio) * 0.80));
            $gold->save();
            $profile_resource->save();
        }
        return $profile_resource ? [$profile_resource->amount, $gold->amount] : false;
    }

    public function rules()
    {
        return [
            [['user_id', 'resource_id'], 'required'],
            [['user_id', 'resource_id', 'needs_time', 'amount'], 'integer'],
            [['resource_id'], 'exist', 'skipOnError' => true, 'targetClass' => Resource::className(), 'targetAttribute' => ['resource_id' => 'resource_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'resource_id' => 'Resource ID',
            'needs_time' => 'Needs Time',
            'amount' => 'Amount',
        ];
    }

    public static function getMiningResource()
    {
        $profile_resource = ProfileResource::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->andWhere('needs_time>0')
            ->limit(1)
            ->one();

        return $profile_resource;
    }

    public function getResource()
    {
        return $this->hasOne(Resource::className(), ['resource_id' => 'resource_id']);
    }

    public function getUser()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }
}
