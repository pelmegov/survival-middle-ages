<?php

namespace app\modules\avatar\controllers;

use app\modules\avatar\models\Avatar;
use app\modules\avatar\models\ChangeAvatarForm;
use app\modules\profile\models\Profile;
use app\modules\profile_resource\models\ProfileResource;
use app\modules\user\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

/**
 * Default controller for the `avatar` module
 */
class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    /* Для всех */
                    [
                        'allow' => true,
                        'controllers' => ['avatar/default'],
                        'actions' => ['test']
                    ],
                    /* Для гостей */
                    [
                        'allow' => true,
                        'controllers' => ['avatar/default'],
                        'actions' => ['test'],
                        'roles' => ['?'],
                    ],
                    /* Для зарегистрированных пользователей */
                    [
                        'allow' => true,
                        'controllers' => ['avatar/default'],
                        'actions' => ['change'],
                        'roles' => ['@'],
                    ],
                    /* Для администраторов */
                    [
                        'allow' => true,
                        'controllers' => ['avatar/default'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->username);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionChange()
    {

        $model = new ChangeAvatarForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->changeAvatar()) {
                Yii::$app->getSession()->addFlash('success', "Аватар успешно обновлен!");
            } else {
                Yii::$app->getSession()->addFlash('error', "Не удалось изменить аватар!");
            }
        }

        $profile = Profile::getProfile();
        $avatars = Avatar::find()->all();

        return $this->render('change', [
            'model' => $model,
            'avatar_id' => $profile->avatar_id,
            'avatars' => $avatars,
        ]);
    }

    public function actionIndex()
    {
        return $this->redirect(['change']);
    }
}
