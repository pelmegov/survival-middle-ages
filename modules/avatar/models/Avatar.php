<?php

namespace app\modules\avatar\models;

use app\modules\profile\models\Profile;
use app\modules\profile_resource\models\ProfileResource;
use Yii;

/**
 * This is the model class for table "{{%avatar}}".
 *
 * @property integer $id
 * @property string $link
 * @property string $name
 * @property integer $cost
 *
 * @property Profile[] $profiles
 */
class Avatar extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%avatar}}';
    }

    public function rules()
    {
        return [
            [['link', 'name'], 'required'],
            [['cost'], 'integer'],
            [['link', 'name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link' => 'Link',
            'name' => 'Name',
            'cost' => 'Cost',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(Profile::className(), ['avatar_id' => 'id']);
    }
}
