<?php

namespace app\modules\avatar\models;

use app\modules\profile\models\Profile;
use app\modules\profile_resource\models\ProfileResource;
use Yii;
use yii\base\Model;

class ChangeAvatarForm extends Model
{
    public $id;

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['id'], 'required'],
        ];
    }

    public function changeAvatar(){
        $profile = Profile::getProfile();
        $avatar = Avatar::findOne($this->id);

        $gold = ProfileResource::findOne([
            "user_id" => $profile->user_id,
            "resource_id" => 1
        ]);

        $isBuy = $gold->amount >= $avatar->cost ? true : false;

        if ($isBuy && $profile->avatar_id != $this->id) {
            $profile->avatar_id = $this->id;
            $gold->amount -= $avatar->cost;
            $gold->save();
            $profile->save(false);

            return true;
        }

        return false;
    }
}