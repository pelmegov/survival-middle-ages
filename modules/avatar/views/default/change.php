<?php

use app\modules\avatar\models\Avatar;
use app\modules\avatar\models\ChangeAvatarForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $avatar_id
 * @var $avatars
 */

$this->title = Yii::t('msg/pages_info', 'Resource Market');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="avatar-default-change">

    <div class="row">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'id')->label(false)->radioList(ArrayHelper::map(Avatar::find()->all(), 'id', 'link'),
            [
                'item' => function ($index, $label, $name, $checked, $value) use ($avatar_id, $avatars) {

                    if ($avatar_id != $value) {
                        $return = '<div class="col-md-3">';
                        $return .= '<label class="modal-radio">';
                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="3">';
                        $return .= '<i></i>';
                        $return .= "<img style='height: 100px;' src='/{$label}' alt=''/>";
                        $return .= "<p>{$avatars[$index]['name']}</p>";
                        $return .= "<p>Стоимость - {$avatars[$index]['cost']} золота</p>";
                        $return .= '</label>';
                        $return .= '</div>';
                    }

                    return $return;
                }
            ]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Изменить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
