<?php
namespace app\modules\user\models;

use app\modules\profile\models\Profile;
use app\modules\profile_resource\models\ProfileResource;
use Yii;
use yii\base\Model;

class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $verifyCode;
    public $nickname;

    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'match', 'pattern' => '#^[\w_-]+$#i'],
            ['username', 'unique', 'targetClass' => User::className(), 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

//            ['verifyCode', 'captcha', 'captchaAction' => '/user/default/captcha'],

            ['nickname', 'filter', 'filter' => 'trim'],
            ['nickname', 'required'],
            ['nickname', 'match', 'pattern' => '/^[a-z]\w*$/i'],
            ['nickname', 'unique', 'targetClass' => Profile::className(), 'message' => 'This nickname has already been taken.'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->status = User::STATUS_WAIT;
            $user->generateAuthKey();
            $user->generateEmailConfirmToken();

            if ($user->save()) {

                $profile = new Profile();
                $profile->user_id = $user->id;
                $profile->nickname = $this->nickname;

                if ($profile->save(false)) {
                    $resource = new ProfileResource();
                    $resource->user_id = $user->id;
                    $resource->resource_id = 1;
                    $resource->needs_time = 0;
                    $resource->amount = Yii::$app->params['defaultGoldAmount'];
                    $resource->save();

                    $resource = new ProfileResource();
                    $resource->user_id = $user->id;
                    $resource->resource_id = 2;
                    $resource->needs_time = 0;
                    $resource->amount = Yii::$app->params['defaultWoodAmount'];
                    $resource->save();

                    $resource = new ProfileResource();
                    $resource->user_id = $user->id;
                    $resource->resource_id = 3;
                    $resource->needs_time = 0;
                    $resource->amount = Yii::$app->params['defaultStoneAmount'];
                    $resource->save();

                    $resource = new ProfileResource();
                    $resource->user_id = $user->id;
                    $resource->resource_id = 4;
                    $resource->needs_time = 0;
                    $resource->amount = Yii::$app->params['defaultAnimalSkinsAmount'];
                    $resource->save();

                    $resource = new ProfileResource();
                    $resource->user_id = $user->id;
                    $resource->resource_id = 5;
                    $resource->needs_time = 0;
                    $resource->amount = Yii::$app->params['defaultFishAmount'];
                    $resource->save();
                }


                Yii::$app->mailer->compose('@app/modules/user/mails/emailConfirm', ['user' => $user])
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                    ->setTo($this->email)
                    ->setSubject('Email confirmation for ' . Yii::$app->name)
                    ->send();
                return $user;
            }
        }

        return null;
    }
}