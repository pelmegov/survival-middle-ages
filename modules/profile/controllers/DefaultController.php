<?php

namespace app\modules\profile\controllers;

use app\modules\profile\models\Profile;
use app\modules\profile_resource\models\ProfileResource;
use app\modules\resource\models\Resource;
use app\modules\user\models\User;
use app\modules\profile\models\EditProfileForm;
use DateTime;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class DefaultController extends Controller
{
    //profile
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    /* Для всех */
                    [
                        'allow' => true,
                        'controllers' => ['profile/default'],
                        'actions' => ['test']
                    ],
                    /* Для гостей */
                    [
                        'allow' => true,
                        'controllers' => ['profile/default'],
                        'actions' => ['view'],
                        'roles' => ['?'],
                    ],
                    /* Для зарегистрированных пользователей */
                    [
                        'allow' => true,
                        'controllers' => ['profile/default'],
                        'actions' => ['index', 'view', 'edit-profile'],
                        'roles' => ['@'],
                    ],
                    /* Для администраторов */
                    [
                        'allow' => true,
                        'controllers' => ['profile/default'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->username);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionEditProfile()
    {
        $model = new EditProfileForm();

        if ($model->load(Yii::$app->getRequest()->post())) {
            if ($model->validate() && $model->save()) {
                Yii::$app->session->addFlash('success', Yii::t('model/profile', 'Profile successfully updated!'));
            } else {
                Yii::$app->session->addFlash('error', Yii::t('model/profile', 'When you update the profile error occurred!'));
            }
        } else {
            $model->loadForm($model);
        }
        return $this->render('edit-profile', ['model' => $model]);
    }


    public function actionWork()
    {

        $request = Yii::$app->request;
        $id = (int)$request->get('id');
        $want_work = (int)$request->get('yes');
        $profile = Profile::getProfile();

        if ($profile->isWork()) {
            $model = ProfileResource::getMiningResource();
        } else {
            // нашли ресурс по id
            $resource = $profile->findMinedResourceById($id);
            // если такого ресурса нету отправили на главную страницу
            if (!$resource) return $this->goHome();

            // нашли ProfileResource данного ресурса
            $model = ProfileResource::findOne([
                'user_id' => Yii::$app->user->id,
                'resource_id' => $resource->resource_id
            ]);

            // Если готов работать
            if ($want_work == 1) {

                // записываем время для работы
                $model->needs_time = time();

                $profile->is_work = 1;
                $profile->save(false);

                // сохраняем модель
                $model->save();

                return $this->actionWork();
            }
        }

        return $this->render('work', ['model' => $model]);
    }

    public function actionIndex()
    {
        $profile = Profile::getProfile();
        $model = Profile::getResourcesByUserId($profile->user_id);

        return $this->render('index', [
            'model' => $model,
            'profile' => $profile
        ]);
    }
}
