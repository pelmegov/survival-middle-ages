<?php

namespace app\modules\profile\models;

use app\modules\profile_resource\models\ProfileResource;
use app\modules\resource\models\Resource;
use app\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "game_profile".
 *
 * @property integer $user_id
 * @property integer $avatar_id
 * @property string $nickname
 * @property string $is_work
 *
 * @property User $user
 */
class Profile extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return '{{%profile}}';
    }

    public function rules()
    {
        return [
            [['avatar_id', 'is_work'], 'integer'],
            [['is_work'], 'safe'],
            [['nickname'], 'required'],
            [['nickname'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('model/profile', 'User ID'),
            'avatar_id' => Yii::t('model/profile', 'Avatar ID'),
            'nickname' => Yii::t('model/profile', 'Nickname'),
            'is_work' => 'Работает ли пользователь',
        ];
    }

    public static function isWork()
    {
        $profile = Profile::getProfile();
        $is_work = $profile->is_work;

        return $is_work ? true : false;
    }

    /**
     * Обновление времени на добычу ресурса
     * @return bool
     */
    public static function updateNeedsTime()
    {
        $profile_resource = ProfileResource::getMiningResource();
        $stay_time = self::howManyStayTime();

        if ($stay_time > 0) {
            $profile_resource->needs_time = $stay_time;
            return $profile_resource->save();
        } else {
            self::endWork();
        }
    }

    private static function endWork()
    {
        $profile_resource = ProfileResource::getMiningResource();

        if (self::isWork()) {
            // прибавили ресурса, который добывали
            $profile_resource->amount += $profile_resource->resource->amount;

            Yii::$app->getSession()->addFlash('success', 'Вы закончили работать и получили '
                . $profile_resource->resource->amount . ' '
                . $profile_resource->resource->resource_name
            );

            // обнулили время
            $profile_resource->needs_time = 0;

            $profile = Profile::getProfile();
            $profile->is_work = 0;

            $profile->save(false);
        } else {
            return true;
        }

        return $profile_resource->save();
    }

    /**
     * Как много времени осталось на добычу ресурса
     * @return int|string
     */
    public static function howManyStayTime()
    {
        $profile_resource = ProfileResource::getMiningResource();
        // Текущее время в минутах
        $now_time = (time() - $profile_resource->needs_time) / 60;
        // Сколько осталось времени
        $stay_time = number_format($profile_resource->resource->needs_time - $now_time, 2);

        if ($stay_time < 0) $stay_time = 0;

        return $stay_time;
    }

    public function findMinedResourceById($id)
    {
        $resource = Resource::find()
            ->where(['resource_id' => $id])
            ->andWhere('needs_time>0')
            ->one();
        return $resource ? $resource : false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfileResources()
    {
        return $this->hasMany(ProfileResource::className(), ['user_id' => 'user_id']);
    }

    public static function getResourcesByUserId($id)
    {
        return ProfileResource::findAll(['user_id' => $id]);
    }

    public static function getProfile()
    {
        return ($profile = Profile::findOne(Yii::$app->user->id)) ? $profile : false;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Функция возвращает массив профилей администраторов
     * @return array|string
     */
    public static function getAdminProfiles()
    {
        $administrators = "";
        $admins = User::getAdmins();
        foreach ($admins as $admin) {
            $administrators[] = Profile::findOne(["user_id" => $admin->id]);
        }
        return $administrators;
    }
}
