<?php
namespace app\modules\profile\models;

use app\modules\profile_resource\models\ProfileResource;
use app\modules\user\models\User;
use Yii;
use yii\base\Model;

class EditProfileForm extends Model
{
    public $nickname;
    public $email;

    public $oldNickname;
    public $oldEmail;

    public function rules()
    {
        $profile = Profile::getProfile();
        $user = User::getUser();

        $this->oldNickname = $profile->nickname;
        $this->oldEmail = $user->email;

        return [
            ['nickname', 'filter', 'filter' => 'trim'],
            ['nickname', 'required'],
            ['nickname', 'match', 'pattern' => '/^[a-z]\w*$/i'],
            ['nickname', 'unique', 'targetClass' => Profile::className(), 'message' => Yii::t('model/profile', 'This nickname is already in use.'), 'filter' => ['!=', 'nickname', $this->oldNickname]],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => Yii::t('model/profile', 'This E-Mail is already in use.'), 'filter' => ['!=', 'email', $this->oldEmail]],
        ];
    }

    public function attributeLabels()
    {
        return [
            'nickname' => Yii::t('model/profile', 'Nickname'),
            'email' => Yii::t('model/profile', 'E-Mail'),
        ];
    }

    public function loadForm($model)
    {
        $profile = Profile::getProfile();
        $user = User::getUser();

        $model->nickname = $profile->nickname;
        $model->email = $user->email;

        return $model;
    }

    public function save()
    {
        $profile = Profile::getProfile();
        $user = User::getUser();

        $profile_resource = ProfileResource::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->andWhere(['resource_id' => 1])
            ->with('resource')
            ->one();

        if ($profile_resource->amount >= Yii::$app->params['nicknameUpdateCost'] && $profile->nickname != $this->nickname) {
            $profile_resource->amount -= Yii::$app->params['nicknameUpdateCost'];
            $profile->nickname = $this->nickname;
            Yii::$app->session->addFlash('success', Yii::t('model/profile', 'Nickname updated successfully!'), false);
        } else if($profile->nickname != $this->nickname) {
            Yii::$app->session->addFlash('error', Yii::t('model/profile', 'Nickname could not be updated ! Check the stock of gold , it should be at least {0, number, integer}', Yii::$app->params['nicknameUpdateCost']), false);
        }

        if ($user->email != $this->email) {
            $user->email = $this->email;
            Yii::$app->session->addFlash('success', Yii::t('model/profile', 'E-Mail has been successfully updated!'), false);
        }

        if ($user->save(false)) {
            if ($profile->save(false) && $profile_resource->save()) {
                return true;
            }
        }
        return false;
    }

}
