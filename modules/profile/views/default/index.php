<?php
use app\modules\profile\models\Profile;
use yii\helpers\Url;

/** @var Profile $profile */
$this->title = Yii::t('msg/pages_info', 'Profile');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="profile-default-index">
    <h1><?= Yii::t('msg/pages_info', 'Profile') . " " . $profile['nickname']; ?></h1>

    <?= Yii::$app->session->getFlash('successProfileUpdate'); ?>

    <p class="bg-success" style="padding: 10px">
        <?= Yii::t('msg/pages_info', 'To change the data:') ?>
        <a href="<?= Url::toRoute(['edit-profile']) ?>"><?= Yii::t('msg/pages_info', 'Change Profile') ?></a>
        <br><br>
        Изменить аватар:
        <a href="<?= Url::toRoute(['/avatar/default/change']) ?>">Изменить</a>
    </p>

    <h2><?= Yii::t('msg/pages_info', 'Stocks:') ?></h2>

    <div class="col-md-6">
        <table class="table">
            <? foreach ($model as $item) : ?>
                <tr>
                    <td><img style="width: 100px" src="/<?= $item['resource']['link_image'] ?>"
                             alt="<?= $item['resource']['resource_name'] ?>"></td>
                    <td><?= $item['resource']['resource_name'] ?></td>
                    <td><?= $item['amount'] ?> ед.</td>
                </tr>
            <? endforeach; ?>

        </table>
    </div>
</div>
