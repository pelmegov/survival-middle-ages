<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('msg/pages_info', 'Change Profile');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'nickname')->textInput() ?>
    <p class="bg-danger" style="padding: 10px; font-weight: bold;">
        <?= Yii::t(
            'msg/pages_info',
            'Attention, change nickname it stands {0, number, integer} gold.',
            Yii::$app->params['nicknameUpdateCost']
        )
        ?>
    </p>

    <div class="form-group">
        <?= Html::submitButton('Редактировать', ['class' => 'btn btn-primary']) ?>
        <a href="<?= Url::toRoute(['index']) ?>"><?= Html::button('В профиль', ['class' => 'btn btn-default']) ?></a>
    </div>
    <?php ActiveForm::end(); ?>

</div>