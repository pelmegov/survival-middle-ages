<?php
use app\modules\profile\models\Profile;
use app\modules\profile_resource\models\ProfileResource;
use app\modules\user\models\User;
use yii\helpers\Url;

$this->title = Yii::$app->name;
?>
<div class="main-default-index">

    <section class="gg-main">
        <? if (!Yii::$app->user->isGuest) : ?>
            <div class="row">
                <div class="col-md-4">
                    <img class="mainimg" src="http://commando.com.ua/uploads/posts/2012-07/1342331264_0076.jpg"
                         alt="<?= Yii::$app->name ?>">

                    <h1 class="imgh1"><?= Yii::$app->name ?></h1>
                </div>

                <? if (Profile::isWork()) : ?>
                    <div class="col-md-8 work">
                        <div>
                            <h2>Тебе еще работать: <span>
                                        <?= Profile::howManyStayTime() ?> минут.
                                    </span>
                            </h2>

                            <p>
                                <br>
                                <a href="<?= Url::to(['/profile/default/work', 'id' => ProfileResource::getMiningResource()->resource_id]) ?>  ">Перейти к детальной информации</a>
                            </p>
                        </div>
                    </div>
                <? else : ?>
                    <div class="col-md-8 no-work">
                        <ul>
                            <li><a href="<?= Url::to(['/profile/default/work', 'id' => 2]) ?>">Хочу рубить лес</a></li>
                            <li><a href="<?= Url::to(['/profile/default/work', 'id' => 3]) ?>">Хочу добывать камень</a></li>
                            <li><a href="<?= Url::to(['/profile/default/work', 'id' => 4]) ?>">Хочу ловить рыбу</a></li>
                            <li><a href="<?= Url::to(['/profile/default/work', 'id' => 5]) ?>">Хочу на охоту</a></li>
                        </ul>
                    </div>
                <? endif ?>
            </div>

            <div class="chat">
                <?= app\widgets\chat\ChatRoom::widget([
                        'url' => \yii\helpers\Url::to(['/main/default/send-chat']),
                        'userModel' => User::className(),
                        'userField' => 'avatarImage'
                    ]
                ); ?>
            </div>

        <? else : ?>
            <div class="row">
                <div class="col-md-12">
                    <h2>Добро пожаловать в игру <?= Yii::$app->name ?></h2>

                    <p>Пожалуйста, пройдите регистрацию чтобы получить доступ к игре.</p>

                    <p>Или войдите, если вы проходили регистрацию ранее.</p>
                </div>
            </div>
        <? endif; ?>
    </section>
</div>