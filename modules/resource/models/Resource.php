<?php

namespace app\modules\resource\models;

use app\modules\profile\models\Profile;
use app\modules\profile_resource\models\ProfileResource;
use Yii;

/**
 * This is the model class for table "{{%resource}}".
 *
 * @property integer $resource_id
 * @property string $resource_name
 * @property integer $needs_time
 * @property integer $amount
 * @property integer $gold_ratio
 * @property string $link_bg_image
 * @property string $link_image
 *
 * @property ProfileResource[] $profileResources
 * @property Profile[] $users
 */
class Resource extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%resource}}';
    }

    public function rules()
    {
        return [
            [['resource_name', 'needs_time', 'amount', 'gold_ratio'], 'required'],
            [['needs_time', 'amount', 'gold_ratio'], 'integer'],
            [['resource_name', 'link_bg_image', 'link_image'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'resource_id' => 'Resource ID',
            'resource_name' => 'Resource Name',
            'needs_time' => 'Needs Time',
            'amount' => 'Amount',
            'gold_ratio' => 'Gold Ratio',
            'link_bg_image' => 'Link Bg Image',
            'link_image' => 'Link Image',
        ];
    }

    public function getProfileResources()
    {
        return $this->hasMany(ProfileResource::className(), ['resource_id' => 'resource_id']);
    }

    public function getUsers()
    {
        return $this->hasMany(Profile::className(), ['user_id' => 'user_id'])->viaTable('{{%profile_resource}}', ['resource_id' => 'resource_id']);
    }
}
