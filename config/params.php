<?php

return [
    'adminEmail' => '',
    'supportEmail' => '',
    'user.passwordResetTokenExpire' => 3600,
    'defaultGoldAmount' => 0,
    'defaultWoodAmount' => 0,
    'defaultStoneAmount' => 0,
    'defaultAnimalSkinsAmount' => 0,
    'defaultFishAmount' => 0,
    'nicknameUpdateCost' => 400,
];
