<?php

return [
    'components' => [
        'db' => [
            'dsn' => 'mysql:host=localhost;dbname=DATABASE_NAME',
            'username' => 'USERNAME',
            'password' => 'PASSWORD',
            'tablePrefix' => 'PREFIX_',
        ],
        'mailer' => [
//            'useFileTransport' => true,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];