<?php

use app\modules\user\models\User;
use yii\helpers\ArrayHelper;

$params = ArrayHelper::merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/local/params-local.php')
);

return [
    'basePath' => dirname(__DIR__),
    'language' => 'en-US',
    'name' => 'Survival: Middle Ages',
    'modules' => [
        'main' => [
            'class' => 'app\modules\main\Module',
        ],
        'user' => [
            'class' => 'app\modules\user\Module',
        ],
        'profile' => [
            'class' => 'app\modules\profile\Module',
        ],
        'resource' => [
            'class' => 'app\modules\resource\Module',
        ],
        'profile_resource' => [
            'class' => 'app\modules\profile_resource\Module',
        ],
        'avatar' => [
            'class' => 'app\modules\avatar\Module',
        ],
        'chat' => [
            'class' => 'app\modules\chat\Module',
        ],
        'task_tracker' => [
            'class' => 'app\modules\task_tracker\Module',
        ],
        'profile_administration' => [
            'class' => 'app\modules\profile_administration\Module',
        ],
    ],
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'charset' => 'utf8',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<controller>/<action>' => '<controller>/<action>',
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
        ],
        'cache' => [
            'class' => 'yii\caching\DummyCache',
        ],
        'log' => [
            'class' => 'yii\log\Dispatcher',
        ],
    ],
    'params' => $params,
];