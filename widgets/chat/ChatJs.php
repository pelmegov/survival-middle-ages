<?php

namespace app\widgets\chat;

use Yii;
use yii\web\AssetBundle;

class ChatJs extends AssetBundle
{

    public $js = [
        'js/chat/chat.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

}
