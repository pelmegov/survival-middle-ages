<?php

namespace app\widgets\chat\models;

use app\modules\avatar\models\Avatar;
use app\modules\profile\models\Profile;
use Yii;

/**
 * This is the model class for table "chat".
 *
 * @property integer $id
 * @property string $message
 * @property integer $userId
 * @property string $updateDate
 */
class Chat extends \yii\db\ActiveRecord
{

    public $userModel;
    public $userField;

    public static function tableName()
    {
        return '{{%chat}}';
    }

    public function rules()
    {
        return [
            [['message'], 'required'],
            [['userId'], 'integer'],
            [['updateDate', 'message'], 'safe']
        ];
    }

    public function getUser()
    {
        if (isset($this->userModel))
            return $this->hasOne($this->userModel, ['id' => 'userId']);
        else
            return $this->hasOne(Yii::$app->getUser()->identityClass, ['id' => 'userId']);
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message' => 'Message',
            'userId' => 'User',
            'updateDate' => 'Update Date',
        ];
    }

    public function beforeSave($insert)
    {
        $this->userId = Yii::$app->user->id;
        return parent::beforeSave($insert);
    }

    public static function records()
    {
        return static::find()->orderBy('id desc')->limit(10)->all();
    }

    public function data()
    {
        $userField = $this->userField;
        $output = '';
        $models = Chat::records();
        if ($models)
            foreach ($models as $model) {

                $nickname = "Unnamed";

                if (isset($model->user)) {
                    $avatar_id = Profile::findOne($model->user->id)->avatar_id;
                    $nickname = Profile::findOne($model->user->id)->nickname;
                    $avatar = Avatar::findOne($avatar_id);
                }

                if (!$avatar) $avatar = Avatar::findOne(1);

                $output .= '
                <div class="row">
                <div class="item">
                <div class="col-md-1">
                    <img style="width: 70px" class="online" alt="user image" src="' . $avatar->link . '">
                </div>
                <div class="col-md-10">
                <p class="message">
                    <a class="name" href="#">
                        <small class="text-muted pull-right" style="color:green"><i class="fa fa-clock-o"></i> ' . \kartik\helpers\Enum::timeElapsed($model->updateDate) . '</small>
                        ' . $nickname . '
                    </a>
                    <br>
                   ' . $model->message . '
                </p>
            </div>
            </div>
            </div>';
            }

        return $output;
    }

}
