<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\modules\profile\models\Profile;
use app\modules\user\models\User;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?php

    Profile::updateNeedsTime();
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    // Guest
    if (Yii::$app->user->isGuest) {
        $menuItems[] = [
            'label' => Yii::t('msg/pages_info', 'Sign Up'),
            'url' => ['/user/default/signup']
        ];
        $menuItems[] = [
            'label' => Yii::t('msg/pages_info', 'Login'),
            'url' => ['/user/default/login']
        ];
        // User
    } else {
        $menuItems[] = [
            'label' => Yii::t('msg/pages_info', 'Profile'),
            'url' => ['/profile/default/index']
        ];

        $menuItems[] = [
            'label' => Yii::t('msg/pages_info', 'Resource Market'),
            'url' => ['#'],
            'items' => [
                [
                    'label' => Yii::t('msg/pages_info', 'Buy Resources'),
                    'url' => ['/profile_resource/default/market-resources', 'action' => 'buy'],
                ],
                [
                    'label' => Yii::t('msg/pages_info', 'Sell Resources'),
                    'url' => ['/profile_resource/default/market-resources', 'action' => 'sell'],
                ],
            ]
        ];

        // Admin
        if (User::isUserAdmin(Yii::$app->user->identity->username)) {

            $menuItems[] = [
                'label' => Yii::t('msg/pages_info', 'Admin Panel'),
                'url' => ['#'],
                'items' => [
                    [
                        'label' => Yii::t('msg/pages_info', 'Task Tracker'),
                        'url' => ['/task_tracker/task-tracker/index']
                    ],
                    [
                        'label' => Yii::t('msg/pages_info', 'User Admin'),
                        'url' => ['/profile_administration/default/index']
                    ],
                ]
            ];

        }
        $menuItems[] = [
            'label' => Yii::t('msg/pages_info', 'Logout') . ' (' . Yii::$app->user->identity->username . ')',
            'url' => ['/user/default/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems
    ]);
    ?>

    <div class="navbar-text pull-right">
        <?=
        \lajax\languagepicker\widgets\LanguagePicker::widget([
            'skin' => \lajax\languagepicker\widgets\LanguagePicker::SKIN_DROPDOWN,
            'size' => \lajax\languagepicker\widgets\LanguagePicker::SIZE_SMALL
        ]);
        ?>
    </div>

    <div class="navbar-text pull-left">
        <? if (Profile::howManyStayTime() > 0) {
            echo "Вам работать - " . Profile::howManyStayTime() . " м.";
        } ?>
    </div>

    <? NavBar::end(); ?>

    <div class="container">

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= Alert::widget() ?>

        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Yii::$app->name ?> <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
