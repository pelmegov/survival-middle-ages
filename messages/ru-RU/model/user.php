<?php
return [
    'Confirm your email address.' => 'Подтвердите ваш электронный адрес.',
    'Thank you! Your Email successfully confirmed.' => 'Спасибо! Ваш Email успешно подтверждён.',
    'Error Confirmation Email.' => 'Ошибка подтверждения Email.',
    'Thank you! Password successfully changed.' => 'Спасибо! Пароль успешно изменён.',
    'Excuse me. We are having problems with sending.' => 'Извините. У нас возникли проблемы с отправкой.',
    'Thank you! In your Email has been sent an email with a link to the password recovery.' => 'Спасибо! На ваш Email было отправлено письмо со ссылкой на восстановление пароля.',
];