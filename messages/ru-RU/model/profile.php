<?php
return [
    'Nickname' => 'Никнейм',
    'E-Mail' => 'E-Mail',
    'User ID' => 'ID юзера',
    'Avatar ID' => 'ID аватарки',
    'E-Mail has been successfully updated!' => 'E-Mail успешно обновлен!',
    'This E-Mail is already in use.' => 'Данный E-Mail уже используется.',
    'Profile successfully updated!' => 'Профиль успешно обновлен!',
    'When you update the profile error occurred!' => 'При обновлении профиля произошли ошибки!',
    'Nickname updated successfully!' => 'Никнейм успешно обновлен!',
    'This nickname is already in use.' => 'Данный никнейм уже используется.',
    'Nickname could not be updated ! Check the stock of gold , it should be at least {0, number, integer}' =>
        'Никнейм не удалось обновить! Проверьте запас золота, его должно быть не меньше {0, number, integer}',
];