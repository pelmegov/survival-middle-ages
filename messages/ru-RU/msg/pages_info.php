<?php
return [
    'Registration' => 'Регистрация',
    'User Admin' => 'Управление пользователями',
    'Survival: Middle Ages' => 'Выживание: Средневековья',
    'Site Name' => 'Имя сайта',

    'Home' => 'Главная',
    'Resource Market' => 'Рынок Ресурсов',
    'Task Tracker' => 'Трекер Задач',
    'Admin Panel' => 'Админка',
    'Buy Resources' => 'Купить ресурсы',
    'Sell Resources' => 'Продать ресурсы',
    'Contact' => 'Контакты',
    'Login' => 'Войти',
    'Logout' => 'Выйти',
    'Error' => 'Ошибка',

    /*work*/
    'Work' => 'Работа',

    /*profile*/
    'Profile' => 'Профиль пользователя',
    'Attention, change nickname it stands {0, number, integer} gold.' => 'Внимание, смена никнейма
        стоит {0, number, integer} золота.',
    'Change Profile' => 'Редактировать профиль',
    'To change the data:' => 'Изменить данные:',
    'Stocks:' => 'Запасы:',

];